#### Home assignment
***

Home assignment for this session is, as follows:

1. Install the following tools:
    * MySQL Community Server
    * MySQL Workbench

2. Write a minimal app that builds a binary tree (in level order) from a given array:
       
   ```bash
   Input  :  arr[] = {1, 2, 3, 4, 5, 6}
   Output : Resulting tree must look like this:
                     1
                   /   \
                  2      3
                 / \    /
                4   5  6
   ```


* Follow the [Way of Working](./way_of_working.md) while implementing the assignment. Preferably, tasks would be developed in either Python, Java, or Javascript.

