### Data Warehouse with AWS Redshift
***

**Let's refresh the difference between the data warehouse and a data lake:**
![](_img/data-warehouse-vs-data-lake-2.png)

**How does a peta-byte scale looks like?**

![](_img/100PB_infographic.png)

Let's refresh the byte units:

![](_img/byte_units.jpg)
#### Row-Oriented Databases
Row-oriented databases typically store whole rows in a physical block. High performance for read operations is achieved through secondary indexes.

Databases such as Oracle Database Server, Microsoft SQL Server, MySQL, and
PostgreSQL are row-oriented database systems. 

These systems have been traditionally used for data warehousing, but they are better suited for **online transactional processing (OLTP)** than for analytics.

To optimize performance of a row-based system used as a data warehouse,
developers use a number of techniques, including 
* building materialized views,
* creating pre-aggregated rollup tables, 
* building indexes on every possible predicate combination, 
* implementing data partitioning to leverage partition pruning by query optimizer, and performing index based joins. 


In a row-based data warehouse, every query has to read through all of the
columns for all of the rows in the blocks that satisfy the query predicate,
including columns you didn’t choose. This approach creates a significant
performance bottleneck in data warehouses, where your tables have more
columns, but your queries use only a few. 

#### Column-Oriented Databases

Column-oriented databases organize each column in its own set of physical blocks instead of packing the whole rows into a block.

This functionality allows them to be more I/O efficient for read-only queries because they only have to
read those columns accessed by a query from disk (or from memory). This approach makes column-oriented databases a better choice than row-oriented
databases for data warehousing.

![](_img/columnar_db.png)

This figure illustrates the primary difference between row-oriented and column-oriented databases. Rows are packed into their own blocks in a row-oriented database, and columns are packed into their own blocks in a column-oriented database.

After faster I/O, the next biggest benefit to using a column-oriented database is
improved compression. Because every column is packed into its own set of
blocks, every physical block contains the same data type. When all the data is the
same data type, the database can use extremely efficient compression algorithms.
As a result, you need less storage compared to a row-oriented database. This
approach also results in significantly lesser I/O because the same data is stored in
fewer blocks.
Some column-oriented databases that are used for data warehousing include
Amazon Redshift, Vertica, Teradata Aster, and Druid.

***
**A versatile example of Columnar-oriented data formats is Apache Parquet**
> Apache Parquet is a columnar storage format available to any project in the Hadoop ecosystem, regardless of the choice of data processing framework, data model or programming language.

> Parquet was created to make the advantages of compressed, efficient columnar data representation available to any project in the Hadoop ecosystem.
  

Please read the anatomy of a Parquet data format in this link: https://parquet.apache.org/documentation/latest/
***

#### Massively Parallel Processing Architectures
Amazon Redshift has a massively parallel processing (MPP) architecture, parallelizing and distributing SQL operations to take advantage of all available resources.

An MPP architecture allows you to use all of the resources available in the cluster
for processing data, thereby dramatically increasing performance of petabytescale data warehouses. 

MPP data warehouses allow you improve performance by simply adding more nodes to the cluster. 
Amazon Redshift, Druid, Vertica, GreenPlum, and Teradata Aster are some of the data warehouses built on an MPP
architecture. Open source frameworks such as Hadoop and Spark also support MPP. 

### AWS Redshift
***
![](_img/redshift_icon.png)  
AmazonRedshift is a fast,fully-managed, petabyte-scale data warehouse service that makes it simple and cost-effective to analyze all your data efficiently using your existing business intelligence tools. It claims to be 75% cheaper than the second popular data warehouse cloud solution.

It is optimized for data sets ranging from a few hundred gigabytes to a petabyte or more, and is designed to cost less than a tenth of the cost of most traditional data warehousing solutions.

Amazon Redshift delivers fast query and I/O performance for virtually any size dataset by using columnar storage technology while parallelizing and distributing queries across multiple nodes.

### AWS Redshift Architecture

This section introduces the elements of the Amazon Redshift data warehouse architecture as shown in the following figure.

![](_img/redshift_arch.png)
Amazon Redshift is a relational database management system (RDBMS), so it is compatible with other RDBMS applications. Although it provides the same functionality as a typical RDBMS, including online transaction processing (OLTP) functions such as inserting and deleting data, Amazon Redshift is optimized for high-performance analysis and reporting of very large datasets.

#### Clusters

The core infrastructure component of an Amazon Redshift data warehouse is a cluster.

A cluster is composed of one or more compute nodes. If a cluster is provisioned with two or more compute nodes, an additional leader node coordinates the compute nodes and handles external communication. 
Your client application interacts directly only with the leader node. The compute nodes are transparent to external applications.

#### Leader node

The leader node manages communications with client programs and all communication with compute nodes. It parses and develops execution plans to carry out database operations, in particular, the series of steps necessary to obtain results for complex queries.

#### Compute nodes

The leader node compiles code for individual elements of the execution plan and assigns the code to individual compute nodes. The compute nodes execute the compiled code and send intermediate results back to the leader node for final aggregation.

Each compute node has its own dedicated CPU, memory, and attached disk storage, which are determined by the node type. As your workload grows, you can increase the compute capacity and storage capacity of a cluster by increasing the number of nodes, upgrading the node type, or both.

Amazon Redshift provides two node types:
* **Dense Storage:** which enables you to create very large data warehouses using hard disk drives (HDDs) for a very low price.
* **Dense Compute:** which enables you to create high-performance data warehouses using fast CPUs, large amounts of RAM, and solid-state disks (SSDs).

You can start with a single 160 GB node and scale up to multiple 16 TB nodes to support a petabyte of data or more.

#### Node slices

A compute node is partitioned into slices. 

Each slice is allocated a portion of the node's memory and disk space, where it processes a portion of the workload assigned to the node.

The leader node manages distributing data to the slices and apportions the workload for any queries or other database operations to the slices. The slices then work in parallel to complete the operation.

When you create a table, you can optionally specify one column as the distribution key. When the table is loaded with data, the rows are distributed to the node slices according to the distribution key that is defined for a table. 

> Choosing a good distribution key enables Amazon Redshift to use parallel processing to load data and execute queries efficiently.

### Designing Tables 
***

#### Distribution Style


Amazon Redshift is optimized for very fast execution of complex analytic queries against very large data sets. Because of the massive amount of data involved in data warehousing, you must specifically design your database to take full advantage of every available performance optimization.

When you load data into a table, Amazon Redshift distributes the rows of the table to each of the node slices according to the table's distribution style.

**Data distribution goals**

Data distribution has two primary goals:

* To distribute the workload uniformly among the nodes in the cluster. Uneven distribution, or data distribution skew, forces some nodes to do more work than others, which impairs query performance.
* To minimize data movement during query execution. If the rows that participate in joins or aggregates are already collocated on the nodes with their joining rows in other tables, the optimizer does not need to redistribute as much data during query execution.

When you create a table, you can designate one of four distribution styles:
* **`EVEN`**,
    * The leader node distributes the rows across the slices in a round-robin fashion, regardless of the values in any particular column. 
    * EVEN distribution is appropriate when a table does not participate in joins.
    ![](_img/even_styl.png)
* **`KEY`**,
    * Tables used in joins
    * Large fact tables in a star schema
    * Distribute data evenly among slices
    * Collocate matching records in the same slice
    ![](_img/key_styl.png)
* **`ALL`**
    * Data that does not change
    * Reasonably sized tables (few millions)
    * No common distribution key
    ![](_img/all_styl.png)
    
#### Sort Keys
AWS Redshift stores the data on disk in a sorted order according to the **sort keys**. Query optimizer uses sort order to determine the optimal query plan.

* Amazon Redshift stores columnar data in 1 MB disk blocks.
* The min and max values for each block are stored as part of the metadata. (Zone maps)

>  For example, if a table stores five years of data sorted by date and a query specifies a date range of one month, up to 98 percent of the disk blocks can be eliminated from the scan. If the data is not sorted

![](_img/sort_keys.png)

Let's analyze this **SQL Statement:** 
```sql
select order_number, customer_id, total_price from order_table where order_date='02/05/2015' 
```

To define a sort type, use either of two keywords with your **CREATE TABLE** statement. 
* **COMPOUND**
    * A compound key is made up of all of the columns listed in the sort key definition, in the order they are listed. A compound sort key is most useful when a query's filter applies conditions, such as filters and joins, that use a prefix of the sort keys. The performance benefits of compound sorting decrease when queries depend only on secondary sort columns, without referencing the primary columns. COMPOUND is the default sort type.
    * Compound sort keys might speed up joins, GROUP BY and ORDER BY operations, and window functions that use PARTITION BY and ORDER BY. For example, a merge join, which is often faster than a hash join, is feasible when the data is distributed and presorted on the joining columns. Compound sort keys also help improve compression.
* **INTERLEAVED**
    * An interleaved sort gives equal weight to each column, or subset of columns, in the sort key. If multiple queries use different columns for filters, then you can often improve performance for those queries by using an interleaved sort style. When a query uses restrictive predicates on secondary sort columns, interleaved sorting significantly improves query performance as compared to compound sorting.
    * The performance improvements you gain by implementing an interleaved sort key should be weighed against increased load and vacuum times.

The default is COMPOUND. An INTERLEAVED sort key can use a maximum of eight columns.

**Create tables without compression, distribution style or sort keys:**
```sql
CREATE TABLE orders_nocomp
(
	o_orderkey BIGINT NOT NULL,
	o_custkey BIGINT NOT NULL,
	o_orderstatus CHAR(1) NOT NULL,
	o_totalprice NUMERIC(12, 2) NOT NULL,
	o_orderdate DATE NOT NULL,
	o_orderpriority CHAR(15) NOT NULL,
	o_clerk CHAR(15) NOT NULL,
	o_shippriority INTEGER NOT NULL,
	o_comment VARCHAR(79) NOT NULL);
```

**Create tables WITH distribution style and sort keys:**

```sql
CREATE TABLE orders
(
	o_orderkey BIGINT NOT NULL DISTKEY,
	o_custkey BIGINT NOT NULL,
	o_orderstatus CHAR(1),
	o_totalprice NUMERIC(12, 2),
	o_orderdate DATE NOT NULL,
	o_orderpriority CHAR(15) NOT NULL,
	o_clerk CHAR(15) NOT NULL,
	o_shippriority INTEGER NOT NULL,
	o_comment VARCHAR(79) NOT NULL
)
SORTKEY
(
	o_orderdate
);
```

#### Designing Tables - Compression Encodings

A compression encoding specifies the type of compression that is applied to a column of data values as rows are added to a table.

If no compression is specified in a CREATE TABLE or ALTER TABLE statement, Amazon Redshift automatically assigns compression encoding as follows:

* Columns that are defined as sort keys are assigned RAW compression.
* Columns that are defined as BOOLEAN, REAL, or DOUBLE PRECISION data types are assigned RAW compression.
* All other columns are assigned LZO compression.


The following table identifies the supported compression encodings and the data types that support the encoding.

![](_img/compression_1.png)

The following statement creates a CUSTOMER table that has columns with various data types. 

**This CREATE TABLE statement** shows one of many possible combinations of compression encodings for these columns.

```sql
create table customer(
custkey int encode delta,
custname varchar(30) encode raw,
gender varchar(7) encode text255,
address varchar(200) encode text255,
city varchar(30) encode text255,
state char(2) encode raw,
zipcode char(5) encode bytedict,
start_date date encode delta32k);
```

**Explanation**
![](_img/redshift_comp_explain.png)


#### Amazon Redshift Spectrum

Spectrum is a feature that enables you to run queries against exabytes of unstructured data in Amazon S3, with no loading or ETL required.

#### Redshift Usage Patterns

Amazon Redshift is ideal for **online analytical processing (OLAP)** using your existing business intelligence tools. 
It is widely used for:
* Run enterprise BI and reporting
* Analyze global sales data for multiple products
* Store historical stock trade data
* Analyze ad impressions and clicks
* Aggregate gaming data
* Analyze social trends
* Measure clinical quality, operation efficiency, and financial performance in health care 

**Anti-Patterns**
* Small datasets 
* OLTP (transactional system)
* Unstructured data
* BLOB data


#### Reference architectures

* **Example 1:**
    ![](_img/Big-Data-Redesign_Diagram_On-Demand-Analytics.png)

* **Example 2:**
    ![](_img/ref_redshift_arch_2.png)


#### Loading Data to Redshift

Redshift **COPY** command is the most efficient way to load a table. You can also add data to your tables using INSERT commands, though it is much less efficient than using COPY.

The COPY command is able to read from multiple data files or multiple data streams simultaneously. Amazon Redshift allocates the workload to the cluster nodes and performs the load operations in parallel, including sorting the rows and distributing data across node slices.


**Exercise 1:** Let's create a table on AWS Redshift and load AWS S3 data to our data warehouse cluster.
***

```sql

create table test_orders (
  id varchar(50) not null distkey sortkey,
  created_at timestamp,
  updated_at timestamp,
  "user" varchar(50),
  source varchar(50),
  categoryname varchar(250),
  totalprice decimal(8,2),
  shippriority varchar(250),
  custkey integer,
  comment varchar(250),
  orderstatus varchar(250),
  orderpriority varchar(250),
  orderkey integer,
  clerk varchar(250)
);


-- show tables
SELECT DISTINCT tablename FROM pg_table_def WHERE  schemaname ='public';

-- Load data to Redshift using COPY command
copy test_orders from 's3://de-prime-testing-bucket/redshift_data/mdp_data.csv' credentials 'aws_iam_role=arn:aws:iam::<your_org_id>:role/<redshift_role>' csv;

-- Queries to check data is loaded
select * from test_orders;
select * from test_orders where categoryname='MOGAS';

-- Drop table
drop table test_orders;

```
