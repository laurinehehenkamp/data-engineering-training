### Spark Streaming
***

Spark Streaming is an extension of the core Spark API that enables scalable, high-throughput, fault-tolerant stream processing of live data streams.

Data can be ingested from many sources like Kafka, Flume, Kinesis, or TCP sockets, and can be processed using complex algorithms expressed with high-level functions like map, reduce, join and window.

![](./_img/streaming-arch.png)

Spark Streaming receives live input data streams and divides the data into batches, which are then processed by the Spark engine to generate the final stream of results in batches.

![](./_img/streaming-flow.png)


#### DStreams
Spark Streaming provides a high-level abstraction called discretized stream or DStream, which represents a continuous stream of data. 

DStreams can be created either from input data streams from sources such as Kafka, Flume, and Kinesis, or by applying high-level operations on other DStreams. 

Internally, a DStream is represented as a sequence of RDDs.

![](_img/streaming-dstream.png)

Any operation applied on a DStream translates to operations on the underlying RDDs. These underlying RDD transformations are computed by the Spark engine.
![](_img/streaming-dstream-ops.png)

##### StreamingContext
To initialize a Spark Streaming program, a StreamingContext object has to be created which is the main entry point of all Spark Streaming functionality.

```java
import org.apache.spark.*;
import org.apache.spark.streaming.api.java.*;
....


SparkConf conf = new SparkConf().setAppName(appName).setMaster(master);
JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(1000));
```

After a context is defined, you have to do the following:
* Define the input sources by creating input DStreams.
* Define the streaming computations by applying transformation and output operations to DStreams.
* Start receiving data and processing it using streamingContext.start().
* Wait for the processing to be stopped (manually or due to any error) using streamingContext.awaitTermination().
* The processing can be manually stopped using streamingContext.stop().

**Gotchas:**
* Once a context has been started, no new streaming computations can be set up or added to it.
* Once a context has been stopped, it cannot be restarted.
* Only one StreamingContext can be active in a JVM at the same time.
* `stop()` on StreamingContext also stops the SparkContext. To stop only the StreamingContext, set the optional parameter of `stop()` called `stopSparkContext` to false.
* A SparkContext can be re-used to create multiple StreamingContexts, as long as the previous StreamingContext is stopped (without stopping the SparkContext) before the next StreamingContext is created.

***

Let's go through the following example and explain the intuition of a Spark Streaming app.
```java
public class StreamingExample {

    public static void main(String[] args) throws InterruptedException {
    
        // Create a local StreamingContext with two working thread and batch interval of 1 second
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(1));

        JavaReceiverInputDStream<String> lines = jssc.socketTextStream("localhost", 9999);

        JavaDStream<String> words = lines.flatMap(x -> Arrays.asList(x.split(" ")).iterator());

        // Count each word in each batch
        JavaPairDStream<String, Integer> pairs = words.mapToPair(s -> new Tuple2<>(s, 1));
        JavaPairDStream<String, Integer> wordCounts = pairs.reduceByKey((i1, i2) -> i1 + i2);

        // Print the first ten elements of each RDD generated in this DStream to the console
        wordCounts.print();

        // Start the computation
        jssc.start();
        // Wait for the computation to terminate
        jssc.awaitTermination();

    }
```

Using Netcat (a small utility in Unix-like systems) let's simulate a data server by using:
```java
nc -lk 9999
```

A JavaStreamingContext object can also be created from an existing JavaSparkContext.

```java
import org.apache.spark.streaming.api.java.*;
.......

// existing JavaSparkContext
JavaSparkContext sc = ...   
JavaStreamingContext ssc = new JavaStreamingContext(sc, Durations.seconds(1));
```

### Input DStreams and Receivers

Input DStreams are DStreams representing the stream of input data received from streaming sources.

Spark Streaming provides two categories of built-in streaming sources.
* Basic sources: Sources directly available in the StreamingContext API. Examples: file systems, and socket connections.
* Advanced sources: Kafka, Flume, Kinesis etc.

##### File Streams

For reading data from files on any file system compatible with the HDFS API (that is, HDFS, S3, NFS, etc.), a DStream can be created as via StreamingContext.fileStream[KeyClass, ValueClass, InputFormatClass].

File streams do not require running a receiver so there is no need to allocate any cores for receiving file data.

For simple text files, the easiest method is StreamingContext.textFileStream(dataDirectory).

```java
// Initializing a file steam from a directory
streamingContext.fileStream<KeyClass, ValueClass, InputFormatClass>(dataDirectory);

// From a text files
JavaDStream<String> lines = streamingContext.textFileStream(dataDirectory);
```

**Exercise 1:**  Create a Spark program to read from a directory and count the words for the newly added file in that directory. 
Let's folder `src/main/resources/streams/` as the streaming source. 
***

##### Advanced Sources

This category of sources require interfacing with external non-Spark libraries, some of them with complex dependencies (e.g., Kafka and Flume). 
Hence, to minimize issues related to version conflicts of dependencies, the functionality to create DStreams from these sources has been moved to separate libraries that can be linked to explicitly when necessary.


Some of these advanced sources are as follows.

* Kafka: Spark Streaming 2.3.3 is compatible with Kafka broker versions 0.8.2.1 or higher. See the [Kafka Integration Guide](https://spark.apache.org/docs/2.3.3/streaming-kafka-integration.html) for more details.
* Flume: Spark Streaming 2.3.3 is compatible with Flume 1.6.0. See the [Flume Integration Guide](https://spark.apache.org/docs/2.3.3/streaming-flume-integration.html) for more details.
* Kinesis: Spark Streaming 2.3.3 is compatible with Kinesis Client Library 1.2.1. See the [Kinesis Integration Guide](https://spark.apache.org/docs/2.3.3/streaming-kinesis-integration.html) for more details.

**Transformations on DStreams**

Some commonly used transformations with DStreams:

* **`filter(func)`**
Return a new DStream by selecting only the records of the source DStream on which func returns true.
 
* **`map(func)`**
Return a new DStream by passing each element of the source DStream through a function func.
    
* **`flatMap(func)`**
Similar to map, but each input item can be mapped to 0 or more output items.

* **`reduceByKey(func, [numTasks])`**
When called on a DStream of (K, V) pairs, return a new DStream of (K, V) pairs where the values for each key are aggregated using the given reduce function. Note: By default, this uses Spark's default number of parallel tasks (2 for local mode, and in cluster mode the number is determined by the config property spark.default.parallelism) to do the grouping. You can pass an optional numTasks argument to set a different number of tasks.

* **`updateStateByKey(func)`**
Return a new "state" DStream where the state for each key is updated by applying the given function on the previous state of the key and the new values for the key. This can be used to maintain arbitrary state data for each key.


#### Checkpointing

A streaming application must operate 24/7 and hence must be resilient to failures unrelated to the application logic (e.g., system failures, JVM crashes, etc.).

For this to be possible, Spark Streaming needs to checkpoint enough information to a fault- tolerant storage system such that it can recover from failures. 

There are two types of data that are checkpointed:
* **Metadata checkpointing** Saving of the information defining the streaming computation to fault-tolerant storage like HDFS. This is used to recover from failure of the node running the driver of the streaming application (discussed in detail later). Metadata includes:
    * Configuration - The configuration that was used to create the streaming application.
    * DStream operations - The set of DStream operations that define the streaming application.
    * Incomplete batches - Batches whose jobs are queued but have not completed yet.
* **Data checkpointing** Saving of the generated RDDs to reliable storage. This is necessary in some stateful transformations that combine data across multiple batches.
In such transformations, the generated RDDs depend on RDDs of previous batches, which causes the length of the dependency chain to keep increasing with time.
***

Spark Streaming stateful example:

```java
public class StatefulStreamingExample {

    public static void main(String[] args) throws InterruptedException {

        Logger.getLogger("org").setLevel(Level.OFF);
        // Batch interval 3ms
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(3));


        // Checkpoint is needed here for updateStateByKey(...) operation
        jssc.checkpoint("tmp/spark");


        // Define the socket where the system will listen
        // Lines is not a rdd but a sequence of rdd, not static, constantly changing
        JavaReceiverInputDStream<String> lines = jssc.socketTextStream("localhost", 9999);

        // Map lines of input data (user:event) into (user, event) pairs
        JavaPairDStream<String, String> events = lines.mapToPair(rawEvent -> {
                    String[] strings = rawEvent.split(":");
                    return new Tuple2<>(strings[0], strings[1]);
                }
        );

        // Print new events received in this batch
        events.foreachRDD((newEventsRdd, time) -> {
            System.out.println("\n===================================");
            System.out.println("New Events for " + time + " batch:");
            for (Tuple2<String, String> tuple : newEventsRdd.collect()) {
                System.out.println(tuple._1 + ": " + tuple._2);
            }
        });

        // Combine new events with a running total of events for each user.
        // userTotals holds pairs of (user, map of event to number of occurrences
        // of that event for that user)
        JavaPairDStream<String, Map<String, Long>> userTotals = events.updateStateByKey(
                (newEvents, oldEvents) -> {
                    Map<String, Long> updateMap = oldEvents.or(new HashMap<>());
                    for (String event : newEvents) {
                        if (updateMap.containsKey(event)) {
                            updateMap.put(event, updateMap.get(event) + 1L);
                        } else {
                            updateMap.put(event, 1L);
                        }
                    }
                    return Optional.of(updateMap);
                });

        userTotals.foreachRDD((userTotals1, time) -> {
            // Instead of printing this would be a good place to do
            // something like writing the aggregation to a database
            System.out.println("");
            System.out.println("Per user aggregate events at " + time + ":");
            // Consider rdd.foreach() instead of collectAsMap()
            for (Map.Entry<String, Map<String, Long>> userData :
                    userTotals1.collectAsMap().entrySet()) {
                System.out.println(String.format("%s: %s",
                        userData.getKey(), userData.getValue()));
            }
        });

        // Start the computation
        jssc.start();
        // Wait for the computation to terminate
        jssc.awaitTermination();

    }
}
```


#### Fault-tolerance Semantics

Spark operates on data in fault-tolerant file systems like HDFS or S3. Hence, all of the RDDs generated from the fault-tolerant data are also fault-tolerant. However, this is not the case for Spark Streaming as the data in most cases is received over the network (except when fileStream is used). To achieve the same fault-tolerance properties for all of the generated RDDs, the received data is replicated among multiple Spark executors in worker nodes in the cluster (default replication factor is 2). 

This leads to two kinds of data in the system that need to recovered in the event of failures:
1. Data received and replicated - This data survives failure of a single worker node as a copy of it exists on one of the other nodes.
2. Data received but buffered for replication - Since this is not replicated, the only way to recover this data is to get it again from the source.


Furthermore, there are two kinds of failures that we should be concerned about:

1. Failure of a Worker Node - Any of the worker nodes running executors can fail, and all in-memory data on those nodes will be lost. If any receivers were running on failed nodes, then their buffered data will be lost.
2. Failure of the Driver Node - If the driver node running the Spark Streaming application fails, then obviously the SparkContext is lost, and all executors with their in-memory data are lost.

**Definitions**
The semantics of streaming systems are often captured in terms of how many times each record can be processed by the system. There are three types of guarantees that a system can provide under all possible operating conditions (despite failures, etc.)

1. At most once: Each record will be either processed once or not processed at all.
2. At least once: Each record will be processed one or more times. This is stronger than at-most once as it ensure that no data will be lost. But there may be duplicates.
3. Exactly once: Each record will be processed exactly once - no data will be lost and no data will be processed multiple times. This is obviously the strongest guarantee of the three.

