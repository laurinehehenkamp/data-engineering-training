### The SQL JOIN statement
***

> Up to this point, we’ve only been working with one table at a time. The real power of SQL, however, comes from working with data from multiple tables at once.

> A JOIN statement is used to combine rows from two or more tables into a single result set.

**The Anatomy of a JOIN statement**

```sql
SELECT table1.column1,table1.column2,table2.column1
FROM table1 
JOIN table2
ON table1.matching_column = table2.matching_column;
```

* The ON keyword allows you to specify the join criteria.
* Join criteria normally uses the equals operator, but other operators may be used
* The INNER and OUTER key words are optional, and may be omitted.

**SQL JOIN Types**

> ![](./_img/sql_joins.png)

<small>Because every SQL tutorial has this picture</small>
***

**SQL INNER JOIN**
> (INNER) JOIN: Returns records that have matching values in both tables

```sql

-- Selecting everything from two tables 
SELECT * FROM
employees emp
 JOIN 
 dept_manager dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments dept
 ON dm.dept_no = dept.dept_no; 
 
-- Chaining more than two tables in a JOIN statement
SELECT * FROM
employees as emp
 JOIN 
 dept_manager as dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments as dept
 ON dm.dept_no = dept.dept_no;
 
-- Selecting only desired columns from the tables
SELECT emp.emp_no, first_name, last_name, dept_name
FROM
employees emp
 JOIN 
 dept_manager dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments dept
 ON dm.dept_no = dept.dept_no;
 
 
-- JOIN query with WHERE Clause
SELECT dept_name, emp.emp_no, first_name, last_name FROM
employees AS emp
 JOIN 
 dept_manager AS dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments AS dept
 ON dm.dept_no = dept.dept_no
WHERE
 dm.to_date = '9999-01-01' 
 AND emp.gender = 'F'
order by dept_name;
```
**SQL NATURAL Join**
> It will join tables where columns of the same name are equal.

```sql
SELECT 
	dept_name, first_name, last_name, title
FROM 
	employees AS emp
		NATURAL JOIN
    dept_manager AS dm
        NATURAL JOIN
    departments AS dept 
        NATURAL JOIN
    titles AS t;
```

**SQL RIGHT JOIN**
> It all records from the right table, and the matched records from the left table. The result is NULL from the left side, when there is no match.

```sql
SELECT * FROM employees as emp
  RIGHT JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;
```

**SQL FULL (OUTER) JOIN**
* It returns all records when there is a match in either left or right table records.
  
```sql
SELECT * FROM employees as emp
  FULL OUTER JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;
```

**SQL LEFT JOIN**

> It returns all records from the left table, and the matched records from the right table. 
The result is NULL from the right side, if there is no match.

```sql
SELECT * FROM employees as emp
  LEFT JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;
```

**SQL UNION JOIN**
> It will combines rows from one or more tables using SELECT statement.
  * Each SELECT statement within UNION must have the same number of columns
  * The columns must also have similar data types
  * The columns in each SELECT statement must also be in the same order

```sql
SELECT 
    dept_name, emp.emp_no, first_name, last_name, 'Manager' as emp_type
FROM
    employees AS emp
        JOIN
    dept_manager AS dm ON emp.emp_no = dm.emp_no
        JOIN
    departments AS dept ON dm.dept_no = dept.dept_no
WHERE
    dm.to_date = '9999-01-01'
UNION
SELECT 
    dept_name, emp.emp_no, first_name, last_name, 'Staff' as emp_type
FROM
    employees AS emp
        JOIN
    dept_emp AS de ON emp.emp_no = de.emp_no
        JOIN
    departments AS dept ON de.dept_no = dept.dept_no
        JOIN
    titles AS t ON t.emp_no = emp.emp_no
WHERE
    de.to_date = '9999-01-01'
        AND t.to_date = '9999-01-01'
ORDER BY emp_type, dept_name, last_name; 
```

**SQL Self JOIN**

> A self JOIN is a regular join, but the table is joined with itself.
```sql
SELECT column_name(s)
FROM employee emp1, employee emp2
WHERE <some_condition>;
```

***

### SQL VIEW Statement

> In SQL, a view is a virtual table based on the result-set of an SQL statement.

> A view contains rows and columns, just like a real table. The fields in a view are fields from one or more real tables in the database.

> You can add SQL functions, WHERE, and JOIN statements to a view and present the data as if the data were coming from one single table.


**The Anatomy of VIEW Statement**

```sql
CREATE VIEW view_name AS
SELECT column1, column2
FROM table_name
WHERE condition;
```

The following SQL statement creates a view that shows:

- Employee first name, last name, customer first name and last name
- Shipper name, product code, product name, order status name, order detail status name
- Tax Status Name

```sql
-- Creates a view with a SELECT query

CREATE VIEW order_data AS
 SELECT 
    employees.first_name as employee_first_name, employees.last_name as employee_last_name, 
    customers.first_name as customer_first_name, customers.last_name as customer_last_name,
    shippers.company as shipper_name, products.product_code, products.product_name, 
    orders_status.status_name as order_status, order_details_status.status_name as order_detail_status,
    orders_tax_status.tax_status_name
FROM
    orders
        JOIN
    order_details ON orders.id = order_details.id
        JOIN
    customers ON orders.customer_id = customers.id
        JOIN
    employees ON orders.employee_id = employees.id
        JOIN
    products ON order_details.product_id = products.id
        JOIN
    orders_tax_status ON ifnull(orders.tax_status_id, 1) = orders_tax_status.id
        JOIN
    orders_status ON orders.status_id = orders_status.id
        JOIN
    order_details_status ON order_details.status_id = order_details_status.id
		JOIN
    shippers ON orders.shipper_id = shippers.id;
    
-- Querying database using a table VIEW
select * from order_data; 

-- Drop the view
DROP VIEW order_data;
```

>  The database engine generates the result every time a user queries a view.

* `CREATE OR REPLACE VIEW` is used to update an existing table view. If it doesn't exist it will create the view.


**Materialized Views**

> Materialized Views are similar to regular database views in the fact they are generated from
physical database tables via a SQL select statement. Currently not natively supported in MySQL, but worth having a clue they exist.


Characteristics:
* Key difference is the result set is actually persisted to disk. 
* Result set is calculated ahead of time and is periodically refreshed.
* Materialized Views are often used for ‘heavy weight’ queries. ie a query which is expensive to execute.
* Ideal for data which needs to be read, which does not change often.

Materialized Views can be refreshed in different kinds:
* never (only once in the beginning, for static data only)
* on demand (for example once a day, for example after nightly load)
* immediately (after each statement)


**Exercise 1:**
Create a table VIEW which returns employees of department `Development`.

