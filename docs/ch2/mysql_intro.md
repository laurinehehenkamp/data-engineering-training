### Getting started with MySQL

**Connecting MySQL Workbench with MySQL Server**

> We're assuming that you installed MySQL Server and Workbench, since it was your home assignment.

* Open MySQL Workbench, navigate to "Database" -> Connect to Database, as follows:
    > <img src="./_img/conn_mysql_1.png" width="500" height="200"> 

* Configure databases connection by following the steps in the figure below:

    > <img src="./_img/conn_mysql_2.png" width="500" height="300">

    
**Creating a database in MySQL**
```sql
create database mydb;
```

**List created DBs**
```sql
show databases;
```


**Using the created database `mydb`**
```sql
use mydb;
```

* Creating a table in MySQL
```sql
create table users(id int, username text);
```
* We're creating table `users` with attributes `username` and `id` of type `text` and `int` inside DB `mydb`.
    * `text` and `int` are built-in MySQL data type - more to come on data types in a later section [Data Types]().

* List created tables within `mydb` database**
```sql
show tables;
```
* Describe what's within table `users` - what attributes it contains, and its schema details.
```sql
desc users;
```

* Now that we created the table, let's add some data to it:
```sql
insert into users(id, username) values(1, 'endrit-b'), (2, 'guest_user');
```

* Let's check what data do we have in database:
```sql
select * from users;
```

* Enough of this `users` table for now, let's delete it:
```sql
drop table users;
```

***
#### A touch of preparation

Before jumping into SQL queries let's get some test data that we're going to use during this chapter.

* Step 1: Open link to download test data repository: https://gitlab.com/endrit-b/test_db
    > ![Test DB](./_img/download_testdb.png)

* Step 2: Unzip the folder and Open MySQL Workbench
* Step 3: Navigate to File -> "Run SQL Script..."

    > <img src="./_img/run_sql_import.png" width="500" height="200">
    

* Step 4: Select file `employees.sql` then click button "Run" and wait for the script to finish.

    > <img src="./_img/run_sql_import_2.png" width="500" height="300">


* This script imports the following tables to databases `employees`:

    > ![](./_img/erd_imports.png)


**Exercise 1:** Write a query that describes the table `salaries` and gets the total count of each tables.