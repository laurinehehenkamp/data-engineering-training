
### SQL Aggregation Functions
***
> This section will walk through SQL Aggregation queries used to group and summarize data.


**The SQL GROUP BY clause** groups rows that have the same values into summary rows, like "find the number of employees in each department".

> GROUP BY is often used with aggregate functions COUNT, MAX, MIN, SUM, AVG
 

```sql
select first_name, count(*) from employees group by first_name;

select birth_date, count(*) from employees group by birth_date;

select salary, count(*) as sal_count from salaries group by salary order by sal_count desc;
```

** SQL GROUP BY with HAVING clause**
> The HAVING clause was added to SQL because the WHERE keyword could not be used with aggregate functions.
 
```sql
select first_name, count(*) as emp_count from employees group by first_name;

select first_name, count(*) as emp_count from employees group by first_name having emp_count > 250;

select salary, count(*) as sal_count from salaries group by salary having sal_count > 100 order by sal_count asc;

-- The one with WHERE and HAVING
select salary, count(*) as sal_count 
from salaries 
where from_date > '1994-06-24'
group by salary 
having sal_count > 50
order by sal_count asc;
```

** SQL GROUP BY with SUM() function**
> SUM() function returns the total sum of a numeric column.

```sql
select date_format(from_date, '%Y') from salaries;

select date_format(from_date, '%Y') as year, sum(salary) from salaries group by year;
```

** SQL GROUP BY with MIN() & MAX() function**
> The MIN() function returns the smallest value of the selected column.

> The MAX() function returns the largest value of the selected column.

```sql
select date_format(from_date, '%Y') from salaries;

select date_format(from_date, '%Y') as year, sum(salary) from salaries group by year;

-- The one with a SQL sub-query
SELECT 
    salary_year, max_salary, min_salary, max_salary - min_salary as delta
FROM
    (SELECT 
        DATE_FORMAT(from_date, '%Y') AS salary_year,
            MAX(salary) AS max_salary,
            MIN(salary) AS min_salary
    FROM
        salaries
    GROUP BY salary_year) as sub_select;
```

** SQL GROUP BY with AVG() function**
> The AVG() function returns the average value of a numeric column.

```sql
SELECT 
    DATE_FORMAT(from_date, '%Y') AS salary_year,
    AVG(salary) AS avg_salary,
    COUNT(*) AS total_recs,
    SUM(salary) AS salary_sum
FROM
    salaries
GROUP BY salary_year;


SELECT 
    avg_salary, salary_sum / total_recs AS calculated_avg
FROM
    (SELECT 
        DATE_FORMAT(from_date, '%Y') AS salary_year,
            AVG(salary) AS avg_salary,
            COUNT(*) AS total_recs,
            SUM(salary) AS salary_sum
    FROM
        salaries
    GROUP BY salary_year) AS subquery;
```