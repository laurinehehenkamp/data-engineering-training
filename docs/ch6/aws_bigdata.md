### AWS BigData Services
***

Amazon Web Services (AWS) provides a broad platform of managed services to
help you build, secure, and seamlessly scale end-to-end big data applications
quickly and with ease. Whether your applications require real-time streaming or
batch data processing, AWS provides the infrastructure and tools to tackle your
next big data project. No hardware to procure, no infrastructure to maintain and
scale—only what you need to collect, store, process, and analyze big data. AWS
has an ecosystem of analytical solutions specifically designed to handle this
growing amount of data and provide insight into your business.

![](_img/aws_big_services.jpg)


The following services for collecting, processing, storing, and analyzing big data
are described in order:
* Amazon S3
* Amazon Redshift
* Amazon Elastic MapReduce (EMR)
* Amazon Glue
* Amazon Athena
* Amazon Kinesis

**What is Serverless?**

Serverless is the native architecture of the cloud that enables you to shift more of your operational responsibilities to AWS, increasing your agility and innovation. Serverless allows you to build and run applications and services without thinking about servers. It eliminates infrastructure management tasks such as server or cluster provisioning, patching, operating system maintenance, and capacity provisioning. You can build them for nearly any type of application or backend service, and everything required to run and scale your application with high availability is handled for you.

**Why use serverless?**

Serverless enables you to build modern applications with increased agility and lower total cost of ownership. Building serverless applications means that your developers can focus on their core product instead of worrying about managing and operating servers or runtimes, either in the cloud or on-premises. This reduced overhead lets developers reclaim time and energy that can be spent on developing great products which scale and that are reliable.


#### What is AWS S3?

![](_img/amazon-s3.png)

Amazon S3 has a simple web services interface that you can use to store and retrieve any amount of data, at any time, from anywhere on the web.

Amazon S3 is a cloud storage used to store objects (files) in “buckets” (directories) 

* **Buckets**
    * Buckets must have a globally unique name
    * Buckets are defined at the region level
    * Naming convention:
        * No uppercase
        * No underscore
        * 3-63 characters long
        * Not an IP
        * Must start with lowercase letter or number
* **Objects**
    * Objects (files) have a Key.The key is the FULL path: 
        * <my_bucket>/my_file.txt
        * <my_bucket>/my_folder1/another_folder/my_file.txt
    * There’s no concept of “directories” within buckets (although the UI will trick you to think otherwise)
    * Just keys with very long names that contain slashes (“/”)
    * Object Values are the content of the body:
        * Max Size is 5TB
        * If uploading more than 5GB, must use “multipart upload”
    * Metadata (list of text key / value pairs – system or user metadata)
    * Tags (Unicode key / value pair – up to 10) – useful for security / lifecycle
    * Version ID (if versioning is enabled)


**AWS S3 - Versioning**

* You can version your files in AWS S3
* It is enabled at the bucket level
* Same key overwrite will increment the “version”: 1, 2, 3....
* It is best practice to version your buckets
    * Protect against unintended deletes (ability to restore a version) 
    * Easy roll back to previous version
* Any file that is not versioned prior to enabling versioning will have version “null”

**S3 Encryption for Objects**

* There are 4 methods of encrypting objects in S3
    * SSE-S3: encrypts S3 objects using keys handled & managed by AWS
    * SSE-KMS: leverage AWS Key Management Service to manage encryption keys 
    * SSE-C: when you want to manage your own encryption keys
    * Client Side Encryption

**Encryption in transit (SSL)**
* AWS S3 exposes:
    * HTTP endpoint: non encrypted
    * HTTPS endpoint: encryption in flight
* You’re free to use the endpoint you want, but HTTPS is recommended 
* HTTPS is mandatory for SSE-C
* Encryption in-transit utilizes SSL/TLS

**S3 Storage Tiers**
* Amazon S3 Standard - General Purpose
* Amazon S3 Standard-Infrequent Access (IA)
* Amazon S3 One Zone-Infrequent Access
* Amazon S3 Reduced Redundancy Storage (deprecated) 
* Amazon S3 Intelligent Tiering (new!)
* Amazon Glacier

**S3 Standard – General Purpose**
* High durability (99.999999999%) of objects across multiple AZ
* If you store 10,000,000 objects with Amazon S3, you can on average expect to incur a loss of a single object once every 10,000 years
* 99.99% Availability over a given year
* Sustain 2 concurrent facility failures
* Use Cases: Big Data analytics, mobile & gaming applications, content distribution...

**S3 Lifecycle Rules**
* Set of rules to move data between different tiers, to save storage cost
* Example: General Purpose => Infrequent Access => Glacier
* Transition actions: It defines when objects are transitioned to another storage class.
    * Eg:We can choose to move objects to Standard IA class 60 days after you created them or can move to Glacier for archiving after 6 months
* Expiration actions: Helps to configure objects to expire after a certain time period. S3 deletes expired objects on our behalf
    * Eg: Access log files can be set to delete after a specified period of time
* Can be used to delete incomplete multi-part uploads!

![](_img/s3_classes.png)

**Exercise 1**: Hands-on lab with Amazon S3 and Apache Spark integration
***

#### AWS Glue

![](_img/glue_icon.png)

AWS Glue is a fully managed extract, transform, and load (ETL) service that you
can use to catalog your data, clean it, enrich it, and move it reliably between data
stores. With AWS Glue, you can significantly reduce the cost, complexity, and
time spent creating ETL jobs. 

AWS Glue is Serverless, so there is no infrastructure to setup or manage. You pay only for the resources consumed
while your jobs are running.


* AWS Glue simplifies many tasks when you are building a data warehouse:

    * Discovers and catalogs metadata about your data stores into a central catalog. You can process semi-structured data, such as clickstream or process logs.
    * Populates the AWS Glue Data Catalog with table definitions from scheduled crawler programs. Crawlers call classifier logic to infer the schema, format, and data types of your data. This metadata is stored as tables in the AWS Glue Data Catalog and used in the authoring process of your ETL jobs.
    * Generates ETL scripts to transform, flatten, and enrich your data from source to target.
    * Detects schema changes and adapts based on your preferences.
    * Triggers your ETL jobs based on a schedule or event. You can initiate jobs automatically to move your data into your data warehouse. Triggers can be used to create a dependency flow between jobs.
    * Gathers runtime metrics to monitor the activities of your data warehouse.
    * Handles errors and retries automatically.
    * Scales resources, as needed, to run your jobs.
    
**Ideal Usage Patterns**

AWS Glue is designed to easily prepare data for extract, transform, and load
(ETL) jobs. Using AWS Glue gives you the following benefits:
* AWS Glue can automatically crawl your data and generate code to
execute or data transformations and loading processes.
* Integration with services like Amazon Athena, Amazon EMR, and
Amazon Redshift
* Serverless, no infrastructure to provision or manage
* AWS Glue generates ETL code that is customizable, reusable, and
portable, using familiar technology – Python and Spark.

**Performance**

AWS Glue uses a scale-out Apache Spark environment to load your data into its
destination. You can simply specify the number of Data Processing Units
(DPUs) that you want to allocate to your ETL job. An AWS Glue ETL job
requires a minimum of 2 DPUs. By default, AWS Glue allocates 10 DPUs to each
ETL job. Additional DPUs can be added to increase the performance of your
ETL job. Multiple jobs can be triggered in parallel or sequentially by triggering
them on a job completion event. You can also trigger one or more AWS Glue
jobs from an external source such as an AWS Lambda function.


**Scalability and Elasticity**

AWS Glue provides a managed ETL service that runs on a Serverless Apache
Spark environment. This allows you to focus on your ETL job and not worry
about configuring and managing the underlying compute resources. AWS Glue
works on top of the Apache Spark environment to provide a scale-out execution
environment for your data transformation jobs.

**Reference Architecture:**

![](_img/glue_arch_ref.png)

#### AWS Athena

![](_img/glue_icon.png)

Amazon Athena is an interactive query service that makes it easy to analyze data
in Amazon S3 using standard SQL. Athena is serverless, so there is no
infrastructure to setup or manage, and you can start analyzing data
immediately. You don’t need to load your data into Athena, as it works directly
with data stored in S3. Just log into the Athena Console, define your table
schema, and start querying. Amazon Athena uses Presto with full ANSI SQL
support and works with a variety of standard data formats, including CSV,
JSON, ORC, Apache Parquet, and Apache Avro. 


**Ideal Usage Patterns**

* Interactive ad hoc querying for web logs – Athena is a good tool
for interactive one-time SQL queries against data on Amazon S3. For
example, you could use Athena to run a query on web and application
logs to troubleshoot a performance issue. You simply define a table for
your data and start querying using standard SQL. Athena integrates with
Amazon QuickSight for easy visualization.
* To query staging data before loading into Redshift – You can
stage your raw data in S3 before processing and loading it into Redshift,
and then use Athena to query that data.
* Send AWS Service logs to S3 for Analysis with Athena –
CloudTrail, Cloudfront, ELB/ALB and VPC flow logs can be analyzed 
Amazon Web Services – Big Data Analytics Options on AWS
Page 43 of 56
with Athena. AWS CloudTrail logs include details about any API calls
made to your AWS services, including from the console. CloudFront logs
can be used to explore users’ surfing patterns across web properties
served by CloudFront. Querying ELB/ALB logs allows you to see the
source of traffic, latency, and bytes transferred to and from Elastic Load
Balancing instances and backend applications. VPC flow logs capture
information about the IP traffic going to and from network interfaces in
VPCs in the Amazon VPC service. The logs allow you to investigate
network traffic patterns and identify threats and risks across your VPC
estate.
* Building Interactive Analytical Solutions with notebook-based
solutions, e.g., RStudio, Jupyter, or Zeppelin - Data scientists and
Analysts are often concerned about managing the infrastructure behind
big data platforms while running notebook-based solutions such as
RStudio, Jupyter, and Zeppelin. Amazon Athena makes it easy to analyze
data using standard SQL without the need to manage infrastructure.
Integrating these notebook-based solutions with Amazon Athena gives
data scientists a powerful platform for building interactive analytical
solutions.


**Performance**

You can improve the performance of your query by compressing, partitioning,
and converting your data into columnar formats. Amazon Athena supports open
source columnar data formats such as Apache Parquet and Apache ORC.
Converting your data into a compressed, columnar format lowers your cost and
improves query performance by enabling Athena to scan less data from S3 when
executing your query.


**Scalability and Elasticity**

Athena is serverless, so there is no infrastructure to setup or manage, and you
can start analyzing data immediately. Since it is serverless it can scale
automatically, as needed.

**Reference architecture of AWS S3, Glue and Athena**

![](_img/glue_ref_arch.png)


**Exercise 1:** Hands-on lab with AWS Glue and Athena.
***

* An example of creatting an extternal table in AWS Athena, pointing to an AWS S3 bucket:

```java
CREATE EXTERNAL TABLE `table_name`(
  `level` string, 
  `datetime` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://<bucket-name>/raw'
TBLPROPERTIES (
  'has_encrypted_data'='false', 
  'skip.header.line.count'='1')
```
