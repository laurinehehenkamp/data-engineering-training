package io.goprime.training;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.Dataset;

import static org.apache.spark.sql.functions.*;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Playground {
    public static void main(String[] args) throws Exception {
        /*
         * This class is dedicated for exercising task during the training session
         ***/

        Logger.getLogger("org").setLevel(Level.ERROR);
        SparkConf conf = new SparkConf().setAppName("PlaygroundApp").setMaster("local[3]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> arr1 = Arrays.asList(1,2,3,3,4);
        List<Integer> arr2 = Arrays.asList(4,5,6);

        JavaRDD<Integer> rdd1 = sc.parallelize(arr1);
        rdd1.foreach(num -> System.out.println(num));

    }
}
