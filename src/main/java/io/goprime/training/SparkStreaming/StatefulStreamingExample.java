package io.goprime.training.SparkStreaming;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.spark.api.java.Optional;

public class StatefulStreamingExample {

    public static void main(String[] args) throws InterruptedException {

        Logger.getLogger("org").setLevel(Level.OFF);
        // Batch interval 3ms
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(3));


        // Checkpoint is needed here for updateStateByKey(...) operation
        jssc.checkpoint("tmp/spark");


        // Define the socket where the system will listen
        // Lines is not a rdd but a sequence of rdd, not static, constantly changing
        JavaReceiverInputDStream<String> lines = jssc.socketTextStream("localhost", 9999);

        // Map lines of input data (user:event) into (user, event) pairs
        JavaPairDStream<String, String> events = lines.mapToPair(rawEvent -> {
                    String[] strings = rawEvent.split(":");
                    return new Tuple2<>(strings[0], strings[1]);
                }
        );

        // Print new events received in this batch
        events.foreachRDD((newEventsRdd, time) -> {
            System.out.println("\n===================================");
            System.out.println("New Events for " + time + " batch:");
            for (Tuple2<String, String> tuple : newEventsRdd.collect()) {
                System.out.println(tuple._1 + ": " + tuple._2);
            }
        });

        // Combine new events with a running total of events for each user.
        // userTotals holds pairs of (user, map of event to number of occurrences
        // of that event for that user)
        JavaPairDStream<String, Map<String, Long>> userTotals = events.updateStateByKey(
                (newEvents, oldEvents) -> {
                    Map<String, Long> updateMap = oldEvents.or(new HashMap<>());
                    for (String event : newEvents) {
                        if (updateMap.containsKey(event)) {
                            updateMap.put(event, updateMap.get(event) + 1L);
                        } else {
                            updateMap.put(event, 1L);
                        }
                    }
                    return Optional.of(updateMap);
                });

        userTotals.foreachRDD((userTotals1, time) -> {
            // Instead of printing this would be a good place to do
            // something like writing the aggregation to a database
            System.out.println("");
            System.out.println("Per user aggregate events at " + time + ":");
            // Consider rdd.foreach() instead of collectAsMap()
            for (Map.Entry<String, Map<String, Long>> userData :
                    userTotals1.collectAsMap().entrySet()) {
                System.out.println(String.format("%s: %s",
                        userData.getKey(), userData.getValue()));
            }
        });

        // Start the computation
        jssc.start();
        // Wait for the computation to terminate
        jssc.awaitTermination();

    }
}
