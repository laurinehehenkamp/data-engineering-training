package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.date_format;

public class S3DatasetExample {

    public static void main(String[] args) {

        final String AWS_ACCESS_KEY = "AKIAVBLPHT7LMUABUVUG";
        final String AWS_SECRET_KEY = "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";

        // Instatiate Spark session object
        SparkSession spark = SparkSession.builder()
                .appName("S3SqlTest")
                // .master("local[*]")
                .config("fs.s3a.access.key", AWS_ACCESS_KEY)
                .config("fs.s3a.secret.key", AWS_SECRET_KEY)
                .config("fs.s3a.endpoint", "s3.us-west-2.amazonaws.com")
                .config("spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version", "2")
                .config("spark.speculation", "false")
                .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
                .getOrCreate();

        // Create Dataset from AWS S3 object
        String inputPath = "s3a://de-prime-testing-bucket/raw/logs.txt";
        Dataset<Row> dataset = spark.read()
                .option("inferSchema", true)
                .option("header", true)
                .csv(inputPath);

        dataset = dataset.select(col("level"), date_format(col("datetime"), "MMMM").alias("month"));

        // count records by log level per month
        dataset = dataset
            .groupBy(col("level"), col("month"))
            .count()
            .orderBy(col("count"));
        dataset.show();

        // Upload data to the S3 data sink
        dataset.write()
                .mode("overwrite")
                .csv("s3a://de-prime-testing-bucket/endrit/sink/");

        spark.stop();
    }
}